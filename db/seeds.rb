# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create! do |u|
  u.email = 'admin@ilpez.tech'
  u.password = 'passwordnyasusahsekali'
  u.author = true
  u.admin = true
end

User.create! do |u|
  u.email = 'author@ilpez.tech'
  u.password = 'passwordnyaagaksusah'
  u.author = true
end

User.create! do |u|
  u.email = 'user@ilpez.tech'
  u.password = 'passwordnyatidaksusah'
end