class AddRolesToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean
    add_column :users, :author, :boolean
    add_column :users, :user, :boolean, default: true
  end
end
