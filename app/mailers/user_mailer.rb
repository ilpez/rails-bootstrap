class UserMailer < ApplicationMailer
  default from: 'admin@ilpez.tech'
  def send_welcome_email(user)
    @url = root_url
    @user = user
    mail(to: @user.email, subject: "Welcome!")
  end
end
