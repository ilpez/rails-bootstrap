class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  after_create :send_admin_mail
  has_many :articles, dependent: :destroy

  def send_admin_mail
    pattern = /@ilpez.tech/
    unless email =~ pattern
      UserMailer.send_welcome_email(self).deliver_later
    end
  end

  rails_admin do
    edit do
      field :admin
      field :author
    end
  end
end
