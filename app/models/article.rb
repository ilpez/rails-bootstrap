class Article < ApplicationRecord
  belongs_to :user
  has_one_attached :cover
  validates :title, presence: true, length: {minimum: 5}
  validates :content, presence: true, length: {minimum: 500}
  validates :cover, presence: true

  def self.search(search)
    if search
      where(["content LIKE :key OR title LIKE :key", key: "%#{search}%"])
    else
      all
    end
  end

  rails_admin do
    edit do
      field :cover
      field :title
      field :content
      field :user_id, :hidden do
        default_value do
          bindings[:view]._current_user.id
        end
      end
    end
  end
end
