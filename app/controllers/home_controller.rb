class HomeController < ApplicationController
  skip_before_action :authenticate_user!
  def index
    @articles = Article.order(created_at: :desc).limit(3).offset(3).with_attached_cover
    @headlines = Article.order(created_at: :desc).limit(3).with_attached_cover
  end
  def about
  end
end
