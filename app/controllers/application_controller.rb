class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { render nothing: true, status: :not_found }
      format.html { redirect_to main_app.root_url, alert: exception.message, status: :not_found }
      format.js   { render nothing: true, status: :not_found }
    end
  end

  def after_sign_in_path_for(resource)
    if can? :read, :dashboard
      stored_location_for(resource) || rails_admin_path
    else
      stored_location_for(resource) || root_path
    end
  end
end
