# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

set :repo_url, 'git@gitlab.com:ilpez/rails-bootstrap.git'
set :application, 'portal-berita'
set :user, 'deploy'
# set :rbenv_type, :user
# set :rbenv_ruby, '2.7.1'
set :deploy_via, :remote_cache
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
# set :puma_threads, [4, 16]
# set :puma_workers, 0

# Don't change these unless you know what you're doing
# set :puma_restart_command, 'bundle exec --keep-file-descriptors puma'
set :puma_bind, "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
# set :puma_preload_app, true
# set :puma_worker_timeout, nil
set :puma_init_active_record, true # Change to false when not using ActiveRecord
# set :migration_role, :app
set :conditionally_migrate, true

set :nginx_ssl_certificate, "/etc/ssl/certs/ssl-cert-snakeoil.pem"
set :nginx_ssl_certificate_key, "/etc/ssl/private/ssl-cert-snakeoil.key"
set :nginx_use_ssl, true
set :nginx_downstream_uses_ssl, true

## Defaults:
# set :scm,           :git
# set :branch,        :master
set :format, :pretty
# set :log_level,     :debug
# set :keep_releases, 5

## Linked Files & Directories (Default None):
append :linked_files, 'config/master.key', 'db/production.sqlite3'
append :linked_dirs, "tmp/sockets", "log", "tmp/pids", "storage"

namespace :deploy do
  #
  #   desc "Make sure local git is in sync with remote."
  #   task :check_revision do
  #     on roles(:app) do
  #       unless `git rev-parse HEAD` == `git rev-parse origin/master`
  #         puts "WARNING: HEAD is not the same as origin/master"
  #         puts "Run `git push` to sync changes."
  #         exit
  #       end
  #     end
  #   end
  #
  #   desc 'Update current apps without recompiling assets'
  #   task :update do
  #     on roles(:app) do
  #       set :assets_roles, []
  #       invoke 'deploy'
  #     end
  #   end
  #
  #   desc 'Initial Deploy'
  #   task :initial do
  #     on roles(:app) do
  #       invoke 'deploy'
  #       after 'deploy:finished', 'deploy:seed'
  #     end
  #   end
  #
  desc 'Seed database with admin and guest user'
  task :seed do
    on primary fetch(:migration_role) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:seed'
        end
      end
    end
  end

  desc 'Run rake yarn:install'
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install")
      end
    end
  end
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma